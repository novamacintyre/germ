-- GERMAN TABLES --
CREATE TABLE de (
    word_id INTEGER PRIMARY KEY NOT NULL,
    pos VARCHAR(6) NOT NULL,
    score INTEGER NOT NULL
);

CREATE TABLE de_adjectives (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_adverbs (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_articles (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_interjections (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_misc (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_nouns (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50),
    plural VARCHAR(50),
    gender CHAR(1)
);

CREATE TABLE de_particles (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_prepositions (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_pronouns (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE de_verbs (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50) NOT NULL UNIQUE,
    third VARCHAR(50),
    past VARCHAR(50),
    participle VARCHAR(50),
    subjunctive VARCHAR(50)
);

-- ENGLISH TABLES --
CREATE TABLE en (
    word_id INTEGER PRIMARY KEY NOT NULL,
    pos VARCHAR(6) NOT NULL,
    score INTEGER NOT NULL
);

CREATE TABLE en_adjectives (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_adverbs (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_articles (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_interjections (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_misc (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_nouns (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50),
    plural VARCHAR(50)
);

CREATE TABLE en_particles (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_prepositions (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_pronouns (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50)
);

CREATE TABLE en_verbs (
    word_id INTEGER NOT NULL UNIQUE,
    base VARCHAR(50) NOT NULL UNIQUE,
    third VARCHAR(50),
    past VARCHAR(50),
    participle VARCHAR(50)
);

-- RELATIONSHIP TABLE --
CREATE TABLE deXen (
    de_id INTEGER NOT NULL UNIQUE,
    en_id INTEGER NOT NULL UNIQUE,
    notes TEXT,
    FOREIGN KEY(de_id) REFERENCES de(word_id),
    FOREIGN KEY(en_id) REFERENCES en(word_id)
);
