import argparse
import sys

import decompress_articles as da


def test_german(t, a, d):
    if len(t) == 1:
        d['letter'] += 1
        return -1, d
    if b'==German==' not in a:
        d['no_translation'] += 1
        return -1, d
    if b'de:deprecated spelling' in a or b'obsolete spelling of|de' in a or b'alternative spelling of|de' in a:
        d['deprecated_spelling'] += 1
        return -1, d
    return 1, d


def test_noun(line, d):
    gender = b''
    status = 0

    if b'{{de-noun|' in line or b'{{de-proper noun|' in line:
        gender = line.strip(b'{}').split(b'|')[1]
        status = 1
    elif b'noun' in line and b'de' in line and b'g=' in line:
        gender = line.strip(b'{}').split(b'g=')[1].split(b'|')[0]
        status = 1

    if len(gender) != 1:
        if b'm' in gender:
            gender = b'm'
        elif b'f' in gender:
            gender = b'f'
        elif b'n' in gender:
            gender = b'n'
        elif b'p' in gender:
            gender = b'p'
        else:
            status = -1

    return status, [gender], d


def test_verb(line, d):
    if b'{{de-verb' in line:
        if b'{{de-verb}}' in line:
            d['no_data'] += 1
            return -1, [], d

        parts = line.strip(b'{}').split(b'|')
        new_parts = []
        for elem in parts:
            if any([x in elem for x in [b'class', b'auxillary', b'de-verb']]):
                continue
            if b'[' or b'{' in elem:
                elem = elem.replace(b"'", b'')
                elem = elem.split(b' or ')
                new_elem = []
                for el in elem:
                    for b in [b'[', b']', b'{', b'}']:
                        el = el.replace(b, b'')
                    new_elem.append(el)
                elem = b'/'.join(new_elem)
            new_parts.append(elem)
        parts = new_parts

        if b'subjunctive' in parts[-1]:
            parts[-1] = parts[-1].split(b'=')[-1]
        else:
            parts.append(parts[1])
        return 1, parts, d

    return 0, [], d


test_pos = {
    "verbs": test_verb,
    "nouns": test_noun,
}


def process_pos(pos, words_scores, wd, pos_files, d_processed):
    completed = set()
    words, scores = list(zip(*words_scores))
    for i, w in enumerate(words):
        start_byte = da.get_streambyte_from_title(wd.fn_titles, w, wd.fn_meta)
        if start_byte == -1:
            d_processed['no_entry'] += 1
            continue

        # Get all titles in stream. This means we only have to access any stream once
        titles = da.get_titles_from_streambyte(wd.fn_bytes, start_byte, wd.fn_meta)
        if titles == -1:
            d_processed['no_titles'] += 1
            continue

        # Throw out titles we don't need, get articles from stream
        cap = set(words).intersection(titles)
        cap = cap.difference(completed)
        articles = da.get_articles_from_stream(wd.fn_archive, start_byte, cap, wd.header)
        if articles == -1:
            d_processed['no_articles'] += 1
            print('Error for article title: ', w)
            continue

        for art in articles:
            e, d_processed = test_german(art[0], art[1], d_processed)
            if e < 1: continue
            lines = art[1].split(b'\n')
            parts = []
            for l in lines:
                e, parts, d_processed = test_pos[pos](l, d_processed)
                if e == 1: break

            if e < 1:
                d_processed['no_data'] += 1
                break

            parts = [p.decode("utf-8") for p in parts]
            parts.insert(0, art[0])
            parts.append(scores[words.index(art[0])])
            pos_files["new"][pos].write(f'{",".join(parts)}\n')
            d_processed['completed'] += 1

        completed.update(cap)


class WikiDump:
    def __init__(self, root_dir, dumpname):
        self.fn_archive = root_dir + dumpname + '.xml.bz2'
        self.fn_titles = root_dir + dumpname + '_titles.txt'
        self.fn_bytes = root_dir + dumpname + '_bytes.txt'
        self.fn_meta = root_dir + dumpname + '_meta.txt'
        self.fn_header = root_dir + dumpname + '_root.txt'
        self.header = da.create_header(self.fn_header)


def main(args):
    root = args.wikt_dir
    dump = args.dump_name
    wd = WikiDump(root, dump)
    raw_dir = args.raw_dir
    out_dir = args.out_dir

    poses = [
        # "adjectives",
        # "adverbs",
        # "articles",
        # "conjunctions",
        # "interjections",
        # "miscellaneous",
        "nouns",
        # "particles",
        # "prepositions",
        # "pronouns",
        "verbs",
    ]

    words_processed = {
        'completed': 0,
        'no_gender': 0,
        'no_start_byte': 0,
        'no_titles': 0,
        'no_translation': 0,
        'no_articles': 0,
        'no_entry': 0,
        'no_data': 0,
        'not_base': 0,
        'only_proper': 0,
        'deprecated_spelling': 0,
        'key_error': 0,
        'letter': 0
    }

    pos_files = {"old": {}, "new": {}}
    for pos in poses:
        pos_files["old"][pos] = open(f'{raw_dir}{pos}.txt', 'r')
        pos_files["new"][pos] = open(f'{out_dir}processed_{pos}.txt', 'w')

    for pos in poses:
        words = pos_files["old"][pos].readlines()

        tmp = []
        for w in words:
            r, s = w.strip().split(',')
            tmp.append((r, s))
        words = tmp

        process_pos(pos, words, wd, pos_files, words_processed)

    print(words_processed)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Get relevant information (e.g., gender) of words")
    parser.add_argument('wikt_dir', help="path: directory containing relevant files for wiktionary parsing")
    parser.add_argument('dump_name', help="str: name of wiktionary dump (e.g."
                                          " enwiktionary-20200820-pages-articles-multistream)")
    parser.add_argument('raw_dir', help="path: directory containing go-processed word lists")
    parser.add_argument('out_dir', help="path: where to write processed files")
    args = parser.parse_args()
    main(args)
