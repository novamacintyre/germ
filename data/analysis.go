package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)


func closeFile(f *os.File) {
	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}


func createFile(fn string) *os.File {
	f, err := os.Create(fn)
	if err != nil {
		log.Fatal(err)
	}
	return f
}


func assignFiles(fileMap map[string]*os.File, poses []string, f *os.File) () {
	/* Assign each part of speech a file descriptor  */
	for _, pos := range poses {
		fileMap[pos] = f
	}
}


func main () {
	// German STTS part of speech tags
	allPos := map[string][]string{
		"adjectives":    {"ADJD", "ADJA"},
		"adverbs":       {"ADV", "PAV", "PAVREL", "PROAV"},
		"articles":      {"ART"},
		"conjunctions":  {"KON", "KOKOM", "KOUS"},
		"interjections": {"ITJ"},
		"miscellaneous": {"PPOSAT", "PRELAT", "PWAT"},
		"nouns":         {"NA", "NN"},
		"particles":     {"PTKANT", "PTKNEG", "PTKREL", "PTKVZ", "PTKZU"},
		"prepositions": {"APPR", "APPO", "APZR", "KOUI"},
		"pronouns":      {"PDS", "PIS", "PPER", "PRF", "PRELS", "PWS", "PWREL"},
		"verbs":         {"VAINF", "VMINF", "VVINF", "VAFIN", "VAIMP", "VMFIN", "VMPP", "VVFIN", "VVIMP", "VVPP"},
	}

	infile, err := os.Open("/home/chloe/Documents/germ/data/100000.freq")
	if err != nil {
		log.Fatal(err)
	}
	defer closeFile(infile)

	posFiles := make(map[string]*os.File)
	for pos, parts := range allPos {
		f := createFile(fmt.Sprintf("/home/chloe/Documents/germ/data/%s.txt", pos))
		defer closeFile(f)
		assignFiles(posFiles, parts, f)
	}

	var base, line, pos string
	baseForms := map[string]bool{}
	mappedPoses := make(map[string][]string)
	var parts []string
	scanner := bufio.NewScanner(infile)
	scores := map[string]float64{}
	var scr float64
	for scanner.Scan() {
		line = scanner.Text()
		parts = strings.Split(line, "\t")
		base, pos = parts[1], parts[2]
		if !baseForms[base] {  // Some bases appear multiple times
			baseForms[base] = true
			mappedPoses[pos] = append(mappedPoses[pos], base)
			scores[base], _ = strconv.ParseFloat(parts[3], 64)
		} else {
			scr, _ = strconv.ParseFloat(parts[3], 64)
			scores[base] += scr
			continue
		}
	}

	var stringScore string
	posStats := map[string]int{}
	for pos, words := range mappedPoses {
		for _, w := range words {
			posStats[pos]++
			hasSymbol := strings.Contains(w, ".")
			if hasSymbol {
				continue
			}
			if posFiles[pos] != nil {
				stringScore = strconv.FormatFloat(scores[w], 'f', -1, 64)
				_, err := posFiles[pos].Write([]byte(w + "," + stringScore + "\n"))
				if err != nil {
					log.Fatal(err)
				}
			}
		}
	}

	fmt.Printf(
		"Parts of speech: %v (%d)\n\n",
		posStats,
		len(posStats),
	)
	fmt.Printf(
		"Number of base forms: %d\n",
		len(baseForms),
	)

}